#!/bin/bash
set -e

if ! (( $1 >= 0 && $1 < 65000)) 2>/dev/null; then
    echo "You should provide valid port number"
    exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${DIR}/.."

npm run build

docker network create stop-network || true

docker build --pull -f Dockerfile-starlette -t stop-starlette:latest .
docker stop stop-starlette || true
docker rm stop-starlette || true
docker run --name=stop-starlette --net=stop-network --restart=always -e BOT_TOKEN="$2" -e CHAT_ID="$3" -d stop-starlette:latest

docker build --pull -f Dockerfile-nginx -t stop-nginx:latest .
docker stop stop-nginx || true
docker rm stop-nginx || true
docker run --name=stop-nginx --net=stop-network --restart=always -p $1:80 -d stop-nginx:latest
