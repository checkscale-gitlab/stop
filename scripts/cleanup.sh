#!/bin/bash
set -e

docker network rm stop-network
docker stop stop-nginx && docker rm stop-nginx
docker stop stop-starlette && docker rm stop-starlette
